(function(){
  /* Dropdown Controller
   Crea un select con estilos personalizados al encontrar la clase selectDrop
   https://www.w3schools.com/howto/howto_custom_select.asp
  */
  var dropdownController = function(){
    var x, i, j, l, ll, selElmnt, a, b, c;
    x = document.getElementsByClassName("selectDrop");
    l = x.length;
    for (i = 0; i < l; i++) {
      selElmnt = x[i].getElementsByTagName("select")[0];
      ll = selElmnt.length;
      
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 1; j < ll; j++) {
        
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
          var y, i, k, s, h, sl, yl;
          s = this.parentNode.parentNode.getElementsByTagName("select")[0];
          sl = s.length;
          h = this.parentNode.previousSibling;
          for (i = 0; i < sl; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
              s.selectedIndex = i;
              h.innerHTML = this.innerHTML;
              y = this.parentNode.getElementsByClassName("same-as-selected");
              yl = y.length;
              for (k = 0; k < yl; k++) {
                y[k].removeAttribute("class");
              }
              this.setAttribute("class", "same-as-selected");
              break;
            }
          }
          h.click();
        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
        
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
        $('.selectDrop select').trigger('change');
      });
    }
    
    function closeAllSelect(elmnt) {
      
      var x, y, i, xl, yl, arrNo = [];
      x = document.getElementsByClassName("select-items");
      y = document.getElementsByClassName("select-selected");
      xl = x.length;
      yl = y.length;
      for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
          arrNo.push(i)
        } else {
          y[i].classList.remove("select-arrow-active");
        }
      }
      for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
          x[i].classList.add("select-hide");
        }
      }
    }
    document.addEventListener("click", closeAllSelect); 
  };
  

  /* Datepicker Controller
   Agrega el datepicker al sitio cuando encuentro la etiqueta indicada
  */
  var datepickerController = function() {
    if($("#txtBirthdate").length > 0){
      $("#txtBirthdate").datepicker({
        dateFormat: 'dd-mm-yy',
        onClose: function() {
          this.focus();
        }
      });
    }
  };

  

  /* ValidateForm Controller
   Realiza la validación del formulario con ayuda de jQuery Validate
  */
  var validateFormController = function() {
    var formElem = $("#form-elem"),
    submitBtn = $(".submit-form"),
    required = $(".required");
    
    required.on("blur", function(){
      if( !$(this).val() ) {
        $(this).addClass('warning');
      } else{
        $(this).removeClass('warning');
      }
      detectRequired();
    });

    $('select').on('change', function() {
      if( !$(this).val() ) {
        $(this).addClass('warning');
      } else{
        $(this).removeClass('warning');
      }
      detectRequired();
    });

    $('input[type="checkbox"]').on("click", function(){
      if( $('input[type="checkbox"]').is(":checked") ) {
        $(this).removeClass('warning');
      } else {
        $(this).addClass('warning');
      }
      detectRequired();
    })



    // Revisar campos requeridos pendientes
    function detectRequired(){
      var allRequired = true;
      required.each(function(){
        if($(this).val() == ''){
          allRequired = false;
        }
      });

      if( !$("input[type=checkbox]").is(":checked") ) {
        allRequired = false;
      }
      
      if(!allRequired){
        submitBtn.prop('disabled', true);
      } else{
        submitBtn.prop('disabled', false);
      }
    }

    // Reglas de validación del formulario
    formElem.validate({
      rules: {
        txtName: {
          required: true,
          maxlength: 100,
          alpha: true
        },
        txtCp: {
          required: true,
          maxlength: 6,
          number: true
        },
        selectSex: {
          required: true
        },
        txtBirthdate: {
          required: true,
          dateFormat: true
        },
        txtEmail: {
          required: true,
          email: true
        },
        txtPhone: {
          required: true,
          number: true,
          maxlength: 10
        },
        txtBrand: {
          required: true,
          noNumbers: true
        },
        txtModel: {
          required: true,
        },
        txtYear: {
          required: true,
          number: true
        },
      },
      errorPlacement: function(error, element) {
        return true;
      },
      submitHandler: function(form) {
        form.submit();
      }
    });

    
    // Método para excluir números
    $.validator.addMethod("alpha", function(value, element) {
      return this.optional(element) || value == value.match(/^[a-zA-Z\s-a-záéíóúÁÉÍÓÚñÑ]+$/);
    });
    
    // Método para el formato de fechas
    $.validator.addMethod(
      "dateFormat",
      function(value, element) {
        var check = false;
        var re = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
        if( re.test(value)){
          var adata = value.split('-');
          var dd = parseInt(adata[0],10);
          var mm = parseInt(adata[1],10);
          var yyyy = parseInt(adata[2],10);
          var xdata = new Date(yyyy,mm-1,dd);
          if ( ( xdata.getFullYear() === yyyy ) && ( xdata.getMonth () === mm - 1 ) && ( xdata.getDate() === dd ) ) {
            check = true;
          }
          else {
            check = false;
          }
        } else {
          check = false;
        }
        return this.optional(element) || check;
      });

    // Método para excluir números e incluir caracteres especiales
    $.validator.addMethod("noNumbers", function(value, element) {
      return this.optional(element) || value == value.match(/[A-Za-záéíóúÁÉÍÓÚñÑ_~\-!@#\$%\^&\*\(\)]+$/);
    });
  };

  var parallaxImageController = function() {
    function isScrolledIntoView(elem) {
      var docViewTop = $(window).scrollTop()*1.1;
      var docViewBottom = (docViewTop + $(window).height())*1.1;
      
      var elemTop = ($(elem).offset().top);
      var elemBottom = (elemTop + $(elem).height());
      
      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    if($(".parallax").length > 0){
      $(window).on('scroll', function() {
        $(".parallax").each(function() {
          var top= 200;
          if (isScrolledIntoView($(this)) && $(this).css("top") > "0") {
            $(this).css({"top": $(this).css("top")});
            $(this).css("top", "-=10")
          }
        });
      });
    }
  }

  var init = function(){
    dropdownController();
    datepickerController();
    validateFormController();
    parallaxImageController();
  };

  init();
})();